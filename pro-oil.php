<!DOCTYPE HTML>
<html>

<head>
    
    <meta charset="utf-8">
    
    <title> AGX—原油</title>
    <meta name="keywords" content="原油,XTIUSD,美国原油,布伦特原油"/>
     <meta name="description" content="原油是世界上最重要的自然资源之一，也是交易最频繁的商品之一。在AGX平台可以进行现货原油交易，即以特定品质的原油作为交易标的物的现货合约，是一种CFD交易，不涉及商品本身，仅以保证金作为风险保障，实行的是T+0交易制度，可以买涨或买跌，无涨跌板限制，风险程度小，交收方式灵活，适合套现。">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=1" />
    
    <!-- favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.png">
    
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="assets/css/jw-base.css" />
    
    <style> 
        .introduce{background: url(assets/img/pro/oil01.jpg) no-repeat right 100px #fbfbfb ;background-size: contain; height: 560px;padding-top: 110px;filter:alpha(opacity=90); -moz-opacity:0.9; -khtml-opacity: 0.9;  opacity: 0.9;  }
        h2{font-size: 35px;}
        p{line-height: 32px;font-size: 15px;color: #666}

        .platform ul li{line-height: 42px; padding-left: 50px; background: url(assets/img/pro/icon.png) no-repeat left 9px;}

        .account-intro{background: url(assets/img/pro/oil03.png) no-repeat;background-size: cover;height: 500px;filter:alpha(opacity=90); -moz-opacity:0.9; -khtml-opacity: 0.9;  opacity: 0.9; }

        @media (max-width: 1440px) {
            .max1440-w920{width: 920px;}
        }

        .bc143a89{background-color: #143a89;}
        .sp-action{background-color: #eee;color: #143a89;}
        .sp-btn-hov:hover{background-color: #eee;color: #143a89;}

        .table th, .table td{text-align: center;}

        @media (max-width: 768px) {
            .max768-tc{text-align: center;}  
            .max768-pl150{padding-left: 150px;}
        }


       

    </style>

</head>

<body>
    
    
    <div id="wrapper" class="ffwryh">
        
        <!-- header -->
        <?php include 'header.html'; ?>
        <div class="container">
            <ol class="breadcrumb bcfff lh50 mb0">
                <li><a href="/">首页</a></li>
                <!-- <li><a href="#">首页</a></li> -->
                <li class="active">原油</li>
            </ol>
        </div>
        <div class="bc333">
            <div class="introduce  ">
                <div class="container"> 
                    <div class="row">   
                        <div class="col-xs-12 col-md-6">    
                            <h2 class="c333 tc">原油</h2>
                            <p class="c333 mt50">原油是世界上最重要的自然资源之一，也是交易最频繁的商品之一。在AGX平台可以进行现货原油交易，即以特定品质的原油作为交易标的物的现货合约，是一种CFD交易，不涉及商品本身，仅以保证金作为风险保障，实行的是T+0交易制度，可以买涨或买跌，无涨跌板限制，风险程度小，交收方式灵活，适合套现。对于投资者来说，投资性比其他类型的交易更强，收益也高。目前AGX平台为客户提供的是市场上广为交易的美国原油（西德克萨斯中质油）和布伦特原油，投资者仅需一个账户就可轻松进行交易。</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="platform container pt50 pb80"> 
            <div class="row">
                <div class="col-xs-12 col-sm-6">    
                    <figure>
                        <img src="assets/img/pro/oil02.jpg" alt="" class="w100- mt30 mb30">
                    </figure>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <h2 class="pl50 mt30 ">为什么选择交易原油？</h2>
                    <ul class="pl50 mt50 fs20 c666">
                        <li>点差低，交易成本低</li>
                        <li>双向交易，有对冲功能</li>
                        <li>无涨跌板限制，风险小</li>
                        <li>价格波动受供需影响，行情容易把握</li>
                        <li>执行迅捷，无重复报价</li>
                        <li>可设止盈止损等多种挂单及指令以控制仓位</li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="data-box pt80 pb80" style="background-color: #f1f1f1">
            <div class="container">
                <h2 class="tc">交易数据</h2>
                <p class="tc plr15 mt30">与AGX进行原油交易，客户能够享受到先进的交易工具，极具竞争力的点差以及的高效的执行速度，无论牛市熊市均有获利的机会。</p>
                <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover c666 mt30" >
                    <tbody>
                        <tr>
                            <td>产品名称</td>
                            <td>最小交易手数</td>
                            <td>最大交易手数</td>
                            <td>合约单位</td>
                            <td>挂单距离</td>
                            <td>保证金比例<br>（视账户净值及余额情况而定）</td>
                            <td>交易时间（北京时间）</td>
                            <td>交易时间（MT4时间）</td>
                        </tr>
                        <tr>
                            <td>USO/USD</td>
                            <td>0.1</td>
                            <td>100</td>
                            <td>1,000</td>
                            <td>5</td>
                            <td>2%</td>
                            <td>周一至周五：07:00-05:59</td>
                            <td>周一至周五：01:00-24:00</td>
                        </tr>
                        <tr>
                            <td>UKO/USD</td>
                            <td>0.1</td>
                            <td>100</td>
                            <td>1,000</td>
                            <td>5</td>
                            <td>2%</td>
                            <td>周一至周五：09:00-05:59</td>
                            <td>周一至周五：03:00-24:00</td>
                        </tr>           
                    </tbody>
                </table>
                </div>

                <p>注：*美国夏令时，北京交易时间相应提前1小时<br>AGX 提醒您考虑提高杠杆率的风险。市场上相对较小的波动可能按比例放大，对您已存入或将要存入的资金产生较大影响，这可能对您不利，也可能对您有利。您可能损失全部原始保证金，并需要存入额外资金来补仓。</p>
                
            </div>
        </div>
        <div class="bc000">
            <div class="account-intro c333 tc">
                <div class="container"> 
                    <div class="row">   
                        <div class="col-xs-12 col-md-6 col-md-offset-6">    
                            <h2 style="margin-top: 150px;">账户介绍</h2>
                            <p class="c333 mt50 fs20">今天，轻松获取最适合您的账户类型</p>
                            <a href="#" class="dib mt50 c333 ">了解更多</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        
        <!-- footer -->
        <?php include 'footer.html'; ?>        
        
    </div> <!-- wrapper -->


    <!-- js -->
    <script src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>   

    <script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <script src="assets/js/jw-base.js"></script>
</body>
</html>