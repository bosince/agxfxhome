function appAjax(params, callback) {
    $.ajax({
        data:    params.data || '',
        type:    params.type || 'POST',
        async:   params.async || true,
        dataType:params.dataType || 'JSON',
        url:     params.url || window.location.href,
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
        success: function(response, textStatus) {
            if(response.status == 'success') {
                if(callback && typeof(callback) === "function") {
                    callback(response.data);
                } else {
                    swal(response.data);
                }
            } else {
                swal('<span class="text-red">'+response.status_code+'</span>错误', response.message, 'error');
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            var msg,
                res = XMLHttpRequest.responseText,
                status = XMLHttpRequest.status;
            switch (status) {
                case 400:
                    msg = '找不到相关内容。';
                    break;
                case 401:
                    msg = '您的操作未经授权';
                    break;
                case 403:
                    msg = '您未被授权访问相关数据。';
                    break;
                case 404:
                    msg = 'URL不是有效的路由，或项目资源不存在。';
                    break;
                case 405:
                    msg = '该方法不被允许。';
                    break;
                case 409:
                    msg = '由于和被请求的资源的当前状态之间存在冲突，请求无法完成。';
                    break;
                case 410:
                    msg = '资料已被删除、停用或暂停...';
                    break;
                case 422:
                    msg = '请求格式正确，但是由于含有语义错误，系统无法响应。';
                    break;
                case 500:
                    msg = '系统发生了意外错误。';
                    break;
                case 503:
                    msg = '系统暂时不可访问，请稍后再试。';
                    break;
                default:
                    msg = '系统发生未知错误。';
            }
            if (res.indexOf('<!DOCTYPE html>') < 0) {
                var json = JSON.parse(res);
                msg = typeof json == 'object' && status == 422
                    ? Object.values(json)[0][0]
                    : res;
            }
            swal('十分抱歉', msg, 'error');
            $('.fa.fa-refresh').removeClass('fa-spin');
        }
    })
}