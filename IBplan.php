<!DOCTYPE HTML>
<html>

<head>
    
    <meta charset="utf-8">
    
    <title>申请成为代理经纪商 - AGX</title>
    
    <!-- meta -->
    <meta name="description" content="AGX ">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=1" />
    
    <!-- favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.png">
    
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="assets/css/jw-base.css" />
    
    <style>
    .slide{ background: url(assets/img/slide/01.jpg);
        background-position: center center;
        background-size: cover;}
    .pt120{padding-top: 120px;}

    .ibslid{
        height: 100vh;
        background: url(assets/img/partner/ib/01.jpg);
        background-attachment: fixed;
    }
    .ibslid a{
        display: inline-block;
        width: 200px;
        height: 50px;
        margin-top: 30px;
        line-height: 45px;
        font-size: 25px;
        border:2px solid #fff;
        border-radius: 5px;
        color: #fff;
    }
    .ibslid a:hover{
        text-decoration: none;
        color: #fff;
        padding-left: 2px;
        border:2px solid #2fc996;
        background-color: #2fc996;
    }
    .h50vh{height: 32vh;}


    .ibintroduce h2{
        text-align: center;
        font-size: 35px;
        margin-bottom: 30px;
    }
    .ibintroduce  p{
        font-size: 17px;
        line-height: 26px;
        margin-top: 10px;
    }

    .ibadvantage figure{
        margin-top: 50px;
        margin-bottom: 15px;
    }
    .ibadvantage p{
        margin-top: 15px;
        font-size: 16px;
        line-height: 28px;
        padding-left: 15px;
        padding-right: 15px;
    }
    .ibservice{
        background: url(assets/img/partner/ib/03.jpg);
        background-size: cover;
    }
    .ibservice li{
        font-size: 20px;
        line-height: 50px;
        font-weight: 700;
    }

    </style>
</head>

<body>
    
    
    <div id="wrapper" class="ffwryh">
        
        <!-- header -->
        <?php include 'header.html'; ?>

        <!-- <div class="slide h300 bcfff pt120 tc ">
            <h2 class="cfff t50- fs40 mt1  ffwryh">申请成为代理经纪商</h2>
            <a href="#" class="btn btn-default w100 h40 lh30 b1sccc">马上申请</a>
        </div> -->
        
        <section class="ibslid tc">
            <div class="h50vh">
            
            </div>
            <h2 class="tc fs80 fw7 cfff ffht">申请成为代理经纪商</h2>
            <a href="http://user.agxfx.com/#/RealAccount" class="">马上申请</a>
        </section>

        <section class="ibintroduce pt60 ">
            <div class="container">

                <h2>客户交易可获得佣金</h2>
                <p>马上加入我们的 介绍经纪商 (IB) 计划, 我们会根据您介绍的客户所产生的每一个交易单子, 给予您丰厚和持续的佣金收入.</p>
                <p>AGX高度重视您所带来的业务, 并致力于为我们的合作伙伴提供最具有吸引力的 IB 经纪回报, 同时为您的客户带来市场领先的交易经验以及最为透明化的交易条件.</p>
                <p>该计划适用于所有个人及各种规模的企业经纪人, 您只需介绍新客户加入 AGX交易就可获得奖励.</p>
                <figure class="mt50">
                    <img src="assets/img/partner/ib/02.png" alt="" class="100-">
                </figure>
                
            </div>
        </section>
        
        <section class="ibadvantage pt60 pb60 bcf1f1f1 tc">
            <div class="container">
                <div class="row">
                    <div class="col-xs-6 col-md-3">
                        <figure>
                            <img src="assets/img/partner/ib/icon01.png" alt="">
                        </figure>
                        <h3>极具吸引力的佣金</h3>
                        <p>我们的佣金回报结构经过精心设计, 以便我们的各级经纪人获得充分的奖励 – 客户交易的数额越多, 您赚取的佣金就越多.</p>
                    </div>
                    <div class="col-xs-6 col-md-3">
                        <figure>
                            <img src="assets/img/partner/ib/icon02.png" alt="">
                        </figure>
                        <h3>先进的报表</h3>
                        <p>先进的 IB 管理中心系统保证各级代理获得完全可视并且及时更新的佣金收入情况和客户交易明细, 并且及时导出报表, 方便管理.</p>
                    </div>
                    <div class="col-xs-6 col-md-3">
                        <figure>
                            <img src="assets/img/partner/ib/icon03.png" alt="">
                        </figure>
                        <h3>先进的营销工具</h3>
                        <p>我们向您提供免费的营销工具和材料, 包括各种图标和网页素材, 为您定制个性化的申请链接. 方便您及时跟踪每一个用户的点击, 展示, 下载和申请.</p>
                    </div>
                    <div class="col-xs-6 col-md-3">
                        <figure>
                            <img src="assets/img/partner/ib/icon04.png" alt="">
                        </figure> 
                        <h3>高级客户经理支持</h3>
                        <p>作为我们重要的合作伙伴, 您将拥有个人专属的一对一 IB 客户经理为您每一步操作提供专业指导, 并且提供业务发展后续支持.</p>
                    </div>
                </div>
            </div>
        </section>

        <section class="ibservice pb60 pt60">
            <div class="container">

                <div class="row">
                    <div class="col-xs-12 col-md-6 pt60">
                        <h3 class="fs40 ">您的客户将获得以下服务</h3>
                        <p class="mt15 fs20 mb30 lh30">当您成为我们的合作伙伴后, 您和您的客户将会获得 AGX提供的最为优质的服务:</p>
                        <ul>
                            <li><span class="glyphicon glyphicon-ok cc33"></span> 最具宽度的全球交易市场</li>
                            <li><span class="glyphicon glyphicon-ok cc33"></span> 好评无数的平台交易系统</li>
                            <li><span class="glyphicon glyphicon-ok cc33"></span> 强大的流动性和执行速度</li>
                            <li><span class="glyphicon glyphicon-ok cc33"></span> 24 小时专业客服技术支持</li>
                            <li><span class="glyphicon glyphicon-ok cc33"></span> 与最可信赖的合规经纪商一起交易</li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        
        <!-- footer -->
        <?php include 'footer.html'; ?>        
        
    </div> <!-- wrapper -->


    <!-- js -->
    <script src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>   

    <script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <script src="assets/js/jw-base.js"></script>
</body>
</html>