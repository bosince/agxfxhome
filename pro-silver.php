<!DOCTYPE HTML>
<html>

<head>
    
    <meta charset="utf-8">
    
    <title> AGX—现货白银</title>
    <meta name="keywords" content="现货白银,XAGUSD"/>
     <meta name="description" content="白银是除了黄金以外另一种颇受欢迎的稀有贵金属，正因其稀缺性及银制产品的日渐升温导致市场对白银的需求上升。白银市场较之于黄金市场规模较小，但是仍然是极具投资价值的物品。与黄金交易特性相同，白银交易也是通过保证金机制进行，并与美元形成此消彼长的货币组合。">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=1" />
    
    <!-- favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.png">
    
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="assets/css/jw-base.css" />
    
    <style> 
        .introduce{background: url(assets/img/pro/silver01.jpg) no-repeat 0 50%;background-size: cover; height: 560px;padding-top: 110px;}
        h2{font-size: 35px;}
        p{line-height: 32px;font-size: 15px;color: #666}

        .platform ul li{line-height: 55px; padding-left: 50px; background: url(assets/img/pro/icon.png) no-repeat left 9px;}

        .account-intro{background: url(assets/img/pro/silver03.jpg) no-repeat 0 80%;background-size: cover;height: 500px;filter:alpha(opacity=90);-moz-opacity:0.9;-khtml-opacity: 0.9;  opacity: 0.9; }

        @media (max-width: 1440px) {
            .max1440-w920{width: 920px;}
        }

        .bc143a89{background-color: #143a89;}
        .sp-action{background-color: #eee;color: #143a89;}
        .sp-btn-hov:hover{background-color: #eee;color: #143a89;}

        .table th, .table td{text-align: center;}

        @media (max-width: 768px) {
            .max768-tc{text-align: center;}  
            .max768-pl150{padding-left: 150px;}
        }
 

    </style>

</head>

<body>
    
    
    <div id="wrapper" class="ffwryh">
        
        <!-- header -->
        <?php include 'header.html'; ?>
        <div class="container">
            <ol class="breadcrumb bcfff lh50 mb0">
                <li><a href="/">首页</a></li>
                <!-- <li><a href="#">首页</a></li> -->
                <li class="active">现货白银</li>
            </ol>
        </div>
        <div class="introduce cfff ">
            <div class="container"> 
                <div class="row">   
                    <div class="col-xs-12 col-md-6">    
                        <h2 class="tl">现货白银</h2>
                        <p class="cfff mt50">白银是除了黄金以外另一种颇受欢迎的稀有贵金属，正因其稀缺性及银制产品的日渐升温导致市场对白银的需求上升。白银市场较之于黄金市场规模较小，但是仍然是极具投资价值的物品。与黄金交易特性相同，白银交易也是通过保证金机制进行，并与美元形成此消彼长的货币组合。当全球经济形势因出现不确定性而导致美元信心动摇时，持有白银同样也是规避风险的一项不错的投资策略。</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="platform container pt50 pb80"> 
            <div class="row">
                <div class="col-xs-12 col-sm-6">    
                    <figure>
                        <img src="assets/img/pro/silver02.jpg" alt="" class="w100- mt30 mb30">
                    </figure>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <h2 class="pl50 mt30 ">为什么选择交易现货白银？</h2>
                    <ul class="pl50  mt50  fs20 c666">
                        <li>具有储备保值功能</li>
                        <li>价格波动大获利机会多</li>
                        <li>可规避美元风险</li>
                        <li>随时交收，容易变现</li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="data-box pt80 pb80" style="background-color: #f1f1f1">
            <div class="container">
                <h2 class="tc">交易数据</h2>
                <p class="tc plr15 mt30">AGX可为客户提供白银交易最高达100:1的杠杆，极具竞争力的点差，国际银行间最优报价，及灵活的合约大小，让客户享受多重指令与挂单的风险管理。</p>
                <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover c666 mt30" >
                    <tbody>
                        <tr>
                            <td>交易货币对</td>
                            <td>最小交易手数</td>
                            <td>最大交易手数</td>
                            <td>合约单位</td>
                            <td>挂单距离</td>
                            <td>保证金比列<br>（视账户净值及余额情况而定）</td>            
                            <td>交易时间（北京时间）</td>
                            <td>交易时间（MT4时间）</td>
                        </tr>
                        <tr>
                            <td>XAG/USD</td>
                            <td>0.01</td>
                            <td>100</td>
                            <td>5,000</td>
                            <td>5</td>            
                            <td>0.5%~2%</td>
                            <td>周一至周五：07:00-05:59</td>
                            <td>周一至周五：01:00-24:00</td>
                        </tr>               
                    </tbody>
                </table>
                </div>

                <p>注：*美国夏令时，北京交易时间相应提前1小时<br>AGX 提醒您考虑提高杠杆率的风险。市场上相对较小的波动可能按比例放大，对您已存入或将要存入的资金产生较大影响，这可能对您不利，也可能对您有利。您可能损失全部原始保证金，并需要存入额外资金来补仓。</p>
                
            </div>
        </div>
        <div class="bc333">
        <div class="account-intro c333 tc">
            <div class="container"> 
                <div class="row">   
                    <div class="col-xs-12 col-md-6 col-md-offset-6">    
                        <h2 style="margin-top: 150px;">账户介绍</h2>
                        <p class="c333 mt50 fs20">今天，轻松获取最适合您的账户类型</p>
                        <a href="#" class="dib mt50 c333">了解更多</a>
                    </div>
                </div>
            </div>
        </div>
        </div>

        
        <!-- footer -->
        <?php include 'footer.html'; ?>        
        
    </div> <!-- wrapper -->


    <!-- js -->
    <script src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>   

    <script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <script src="assets/js/jw-base.js"></script>
</body>
</html>