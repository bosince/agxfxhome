<!DOCTYPE HTML>
<html>

<head>
    
    <meta charset="utf-8">
    
    <title>Index - AGX</title>
    
    <!-- meta -->
    <meta name="description" content="AGX ">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=1" />
    
    <!-- favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.png">
    
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="assets/css/jw-base.css" />
    
    <style> 
        .introduce{background: url(assets/img/pro/indices01.jpg) no-repeat ;background-size: cover; height: 560px;padding-top: 110px;}
        h2{font-size: 35px;}
        p{line-height: 32px;font-size: 15px;color: #666}

        .platform ul li{line-height: 42px; padding-left: 50px; background: url(assets/img/pro/icon.png) no-repeat left 9px;}

        .account-intro{background: url(assets/img/pro/indices03.jpg) no-repeat 0 25%;background-size: cover;height: 500px;}

        @media (max-width: 1440px) {
            .max1440-w920{width: 920px;}
        }

        .bc143a89{background-color: #143a89;}
        .sp-action{background-color: #eee;color: #143a89;}
        .sp-btn-hov:hover{background-color: #eee;color: #143a89;}

        .table th, .table td{text-align: center;}

        @media (max-width: 768px) {
            .max768-tc{text-align: center;}  
            .max768-pl150{padding-left: 150px;}

            .max768-lh23{line-height: 23px;}
            .max768-mt15{margin-top: 15px;}

            .introduce{
                height: 500px;
                padding-top: 60px;
            }
            .max768-pl20{padding-left: 20px;}
            .max768-fs25{font-size: 25px;}
            .max768-fs16{font-size: 16px;}
            .max768-mt20{margin-top: 20px;}

        }
    

    </style>

</head>

<body>
    
    
    <div id="wrapper" class="ffwryh">
        
        <!-- header -->
        <?php include 'header.html'; ?>
        <div class="container">
            <ol class="breadcrumb bcfff lh50 mb0">
                <li><a href="/en">Home</a></li>
                <li class="active">Index</li>
            </ol>
        </div>
        <div class="introduce  ">
            <div class="container"> 
                <div class="row">   
                    <div class="col-xs-12 col-md-6">    
                        <h2 class="c333 tc">Index</h2>
                        <p class="c333 mt50 max768-lh23 max768-mt15">Index of stock index, index reflects price changes in different periods, and the price changes caused by changes in the price of profit or loss by CFD (CFD) to reflect and put forward. Foreign exchange trading, the index is traded in margin, profit or loss by buying and selling price. Compared to the stock index trading, don't allow customers to invest all of the funds can be freely traded, two-way trading market volatility at the same time enjoy the pupil, bring all the benefits and risks of.AGX Angus international can provide six influential world index including the Dow Jones index, the S & P index and the NASDAQ index, the FTSE 100 index Germany, France DAX30 index, CAC40 index.</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="platform container pt50 pb80"> 
            <div class="row">
                <div class="col-xs-12 col-sm-6">    
                    <figure>
                        <img src="assets/img/pro/indices02.jpg" alt="" class="w100- mt30 mb30">
                    </figure>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <h2 class="pl50 max768-pl20  mt30 max768-fs25">Why choose trading Index?</h2>
                    <ul class="pl50 max768-pl20  mt50 max768-mt20 fs20 max768-fs16 c666">
                        <li>Perform fast, no repeat offer</li>
                        <li>Transaction cost is low</li>
                        <li>To hedge by short selling</li>
                        <li>No stamp duty, low cost</li>
                        <li>Multiple single mode, flexible lever</li>
                        <li>Earn dividends</li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="data-box pt80 pb80" style="background-color: #f1f1f1">
            <div class="container">
                <h2 class="tc">transaction data</h2>
                <p class="tc plr15 mt30">AGX allows customers to order moment in the international foreign exchange market, and enjoy it, the market circulation of the top and reasonable leverage and flexible, low transaction costs, no need to pay the settlement costs, profit by the sale price.</p>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover c666 mt30 ">
                        <tbody>
                            <tr>
                                <td>Transaction currency </td>
                                <td>Minimum number<br> of transactions</td>
                                <td>Maximum number<br> of transactions</td>
                                <td>Contract unit</td>
                                <td>Order distance</td>
                                <td>Bail scale </td>
                                <td>Business time<br>（Beijing time）</td>
                                <td>Business time<br>（MT4 time）</td>
                            </tr>
                            <tr>
                                <td>U30USD</td>
                                <td>0.1</td>
                                <td>100</td>
                                <td>10</td>
                                <td>10</td>
                                <td>2%</td>
                                <td>Monday to Thursday：01:00-23:15  23:30-24:00<br>Friday：01:00-23:15</td>
                            </tr>
                            <tr>
                                <td>SPXUSD</td>
                                <td>0.1</td>
                                <td>100</td>
                                <td>100</td>
                                <td>10</td>
                                <td>2%</td>
                                <td>Monday to Thursday：07:00-05:15  05:30-05:59<br>Friday：07:00-05:15</td>
                                <td>Monday to Thursday：01:00-23:15  23:30-24:00<br>Friday：01:00-23:15</td>
                            </tr>
                            <tr>
                                <td>NASUSD</td>
                                <td>0.1</td>
                                <td>100</td>
                                <td>10</td>
                                <td>10</td>
                                <td>2%</td>
                                <td>Monday to Thursday：07:00-05:15  05:30-05:59<br>Friday：07:00-05:15</td>
                                <td>Monday to Thursday：01:00-23:15  23:30-24:00<br>Friday：01:00-23:15</td>
                            </tr>
                            <tr>
                                <td>100GBP</td>
                                <td>0.1</td>
                                <td>100</td>
                                <td>10</td>
                                <td>10</td>
                                <td>4%</td>
                                <td>Weekdays：15:00-05:00</td>
                                <td>Weekdays：09:00-23:00</td>
                            </tr>
                            <tr>
                                <td>D30EUR</td>
                                <td>0.1</td>
                                <td>100</td>
                                <td>10</td>
                                <td>10</td>
                                <td>4%</td>
                                <td>Weekdays：15:00-05:00</td>
                                <td>Weekdays：09:00-23:00</td>
                            </tr>
                            <tr>
                                <td>F40EUR</td>
                                <td>0.1</td>
                                <td>100</td>
                                <td>10</td>
                                <td>10</td>
                                <td>4%</td>
                                <td>Weekdays：15:00-05:00</td>
                                <td>Weekdays：09:00-23:00</td>
                            </tr>
                            <tr>
                                <td>H33HKD</td>
                                <td>0.1</td>
                                <td>100</td>
                                <td>10</td>
                                <td>10</td>
                                <td>2%</td>
                                <td>Weekdays：09:15-12:00  13:15-16:15</td>
                                <td>Weekdays：04:15-07:00  08:15-11:15</td>
                            </tr>
                            <tr>
                                <td>300CNH</td>
                                <td>0.01</td>
                                <td>100</td>
                                <td>300</td>
                                <td>10</td>
                                <td>2%</td>
                                <td>Weekdays：09:30-11:30  13:00-15:00</td>
                                <td>Weekdays：03:30-05:30  07:00-09:00</td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <p>Remark：Daylight saving time in the United States, Beijing trading time corresponding to 1 hours earlier<br>AGX remind you to consider raising the risk leverage. A relatively small market volatility may be scaled up, have a greater impact on you have deposited or will have to deposit funds, it may be bad for you, may also be beneficial to you. You may lose all the original deposit, and need to deposit additional funds to cover short positions.</p>
                
            </div>
        </div>

        <div class="account-intro cfff tc hidden-xs">
            <div class="container"> 
                <div class="row">   
                    <div class="col-xs-12 col-md-6 col-md-offset-6">    
                        <h2 style="margin-top: 150px;">Account</h2>
                        <p class="cfff mt50 fs20">Now, Easy get your account type</p>
                        <a href="#" class="dib mt50 cfff hov-cfff">Read More</a>
                    </div>
                </div>
            </div>
        </div>

        
        <!-- footer -->
        <?php include 'footer.html'; ?>        
        
    </div> <!-- wrapper -->


    <!-- js -->
    <script src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>   

    <script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <script src="assets/js/jw-base.js"></script>
</body>
</html>