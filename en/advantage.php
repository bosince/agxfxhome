<!DOCTYPE HTML>
<html>

<head>

    <meta charset="utf-8">

    <title>AGX—Join Advantage</title>
    <meta name="keywords" content="AGX, Join Advantage "/>
     <meta name="description" content=" AGX Join Advantage ">

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=1" />

    <!-- favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.png">

    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="assets/css/jw-base.css" />

    <!-- load modernizer -->
    <script type="text/javascript" src="assets/js/modernizr/modernizr-2.7.1.js"></script>

    <style>
        .media-img{width: 200px;}
        .media-title{padding-top: 30px;}
        .media-list{
            padding-top: 30px;
            margin-bottom: 50px;
            margin-left: 30px;
            margin-right: 30px;
        }
        .media-list h4{
            font-weight: 700;
            font-size: 20px;
            line-height: 35px;
        }
        .container p{
            font-size: 15px;
            line-height: 28px;
        }
        .media-body{padding-left: 30px;}
        @media (max-width: 1440px) {
            .max1440-w920{width: 920px;}

        }

         @media (max-width: 992px){
            .media-img{width: 150px;}
            .media-title{padding-top: 30px;}
            .media-list{
                padding-top: 30px;
                margin-bottom: 50px;
                margin-left: 0;
                margin-right: 0;
            }
        }
        

        @media (max-width: 768px){
            .media-img{width: 100px;}
            .media-title{padding-top: 30px;}
            .media-list{
                padding-top: 30px;
                margin-bottom: 50px;
                margin-left: 0;
                margin-right: 0;
            }
        }
        

    </style>

</head>

<body>


    <div id="wrapper">

        <!-- header -->
        <?php include 'header.html'; ?>

        <div class="slide h300 bcfff tc pt120">
            <h2 class="cfff fs40  ffwryh">Join Advantage</h2>
        </div>



        <div  >
        <div class="container  ">
            <p class="mt50">AGX international as a professional platform for the financial transactions of foreign exchange services, has been committed to the use of the financial sector and high-end technology products, relying on international service level and professional marketing team, for the partners to provide flexible management mode and the cooperation project, and for a variety of product portfolio, trading mechanism, as well as customer service support, help partners quickly create more reasonable way of marketing, improve corporate profits.</p>
            <ul class="media-list ">
                <li class="media">
                    <div class="media-left media-middle">
                        <img class="media-object media-img" src="assets/img/about/adv/1.jpg" alt="...">
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading media-title">Support for all-round investment varieties</h4>
                        <p>AGX international for partners to provide diversified and comprehensive international investment varieties, including foreign exchange, precious metals, crude oil, stocks, futures and other products, convenient partner service the different needs of customers.</p>
                    </div>
                </li>
                <li class="media">
                    <div class="media-left media-middle">
                        <img class="media-object media-img" src="assets/img/about/adv/2.jpg" alt="...">
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading media-title">Comprehensive plan support</h4>
                        <p>With mature operation experience for many years in the foreign exchange industry, AGX international formed the international competitive agent into plan, can adopt flexible and efficient commission system, plus with partners share dividend system, help partners to expand business better.</p>
                    </div>
                </li>
                <li class="media">
                    <div class="media-left media-middle">
                        <img class="media-object media-img" src="assets/img/about/adv/3.jpg" alt="...">
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading media-title">All-around smart background support</h4>
                        <p>AGX  international has always been committed to provide strong marketing support to partners, to provide partners including intelligent trading terminal, management background, CRM customer management system, intelligent risk control system etc. Strong background support, help partners more smoothly for the customer and money management, and help users more intuitive marketing data.</p>
                    </div>
                </li>
                <li class="media">
                    <div class="media-left media-middle">
                        <img class="media-object media-img" src="assets/img/about/adv/4.jpg" alt="...">
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading media-title">Comprehensive marketing strategy support</h4>
                        <p>AGX international is good at adjust measures to local conditions of international partners in service, for each partner to provide customized scheme of cooperation, and include lectures, training, media advertising, marketing and other marketing support, help partners More targeted to expand the market.</p>
                    </div>
                </li>
                <li class="media">
                    <div class="media-left media-middle">
                        <img class="media-object media-img" src="assets/img/about/adv/5.jpg" alt="...">
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading media-title">Full-service training support</h4>
                        <p>Aimed at understanding degree of different partners in industry and the differences of marketing, AGX international partners to provide professional training services, including foreign exchange knowledge, skills, operation management and after-sales service, etc., to help better business partners.</p>
                    </div>
                </li>
                <li class="media">
                    <div class="media-left media-middle">
                        <img class="media-object media-img" src="assets/img/about/adv/6.jpg" alt="...">
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading media-title">Comprehensive hardware data support </h4>
                        <p>AGX  international worldwide deployment of more high configuration server, and the establishment of a huge cloud data center, help the global and domestic investors highway bridge international banking system, eliminate delays to the millisecond level, in order to clinch a deal quickly.</p>
                    </div>
                </li>
                <li class="media">
                    <div class="media-left media-middle">
                        <img class="media-object media-img" src="assets/img/about/adv/7.jpg" alt="...">
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading media-title">All-around exclusive customer service support</h4>
                        <p>AGX international for global partners including China international flow of dedicated customer service, customer service, and set up a variety of language help partners in the global range analysis of all sorts of problems encountered in the operation, and provide professional Suggestions on problem solving.</p>
                    </div>
                </li>
            </ul>
        </div>
    </div>

        <!-- footer -->
        <?php include 'footer.html'; ?>

    </div> <!-- wrapper -->

    <!-- js -->
    <script src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>

    <script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <script src="assets/js/jw-base.js"></script>

</body>
</html>
