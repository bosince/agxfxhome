<!DOCTYPE HTML>
<html>

<head>
    
    <meta charset="utf-8">
    
    <title> PC AGX MT4 - AGX</title>
    
    <!-- meta -->
    <meta name="description" content="AGX ">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=1" />
    
    <!-- favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.png">
    
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="assets/css/jw-base.css" />
    
    <style> 
        .lSuperiority li{line-height: 32px;}

        @media (max-width: 992px){
            .max992-ml0{margin-left: 0;}
            .max992-ml120{margin-left: 120px;}
            .max992-ml20-{margin-left: 20%;}
            .max992-tc{text-align: center;}
        }
    </style>

</head>

<body>
    
    <div id="wrapper" class="ffwryh">
        
        <!-- header -->
        <?php include 'header.html'; ?>
        <div class="pt60 pb60">
            <div class="container">
                <div class="row ">
                    <div class="col-xs-12 col-md-6 col-md-push-6">
                        <!-- 图片 -->
                        <figure>
                            <img src="assets/img/mt4/pc/01.png" alt="" class="w100-">
                        </figure>
                    </div>
                    <div class="col-xs-12 col-md-6 col-md-pull-6">
                        <!-- 文字 -->
                        <div class=" ml30 max992-ml0 max992-tc">   
                            <h2 class="fs30 fw7 lh35 mt30  ">Why AGX MT4 is Better?</h2>
                            <p class="mt15 ">AGX pioneered the offering of an MT4 platform with trading execution quality in mind. Trade on MT4 with No Requotes, leverage ranging 100:1.</p>
                        </div>
                        <div class="ml30 max992-ml20-">
                            <h3 class="fs20 lh35 mt30 ">AGX MT4 Features</h3>
                            <ul class="lSuperiority  ">    
                                <li><span class="glyphicon glyphicon-ok"></span> Over 55 Instruments Including Forex, CFDs and Futures</li>
                                <li><span class="glyphicon glyphicon-ok"></span> Spreads as low as 0 pips</li>
                                <li><span class="glyphicon glyphicon-ok"></span> Full EA (Expert Advisor) Functionality</li>
                                <li><span class="glyphicon glyphicon-ok"></span> 1 Click Trading</li>
                                <li><span class="glyphicon glyphicon-ok"></span> Technical Analysis Tools with 50 indicators and charting tools</li>
                                <li><span class="glyphicon glyphicon-ok"></span> 3 Chart Types</li>
                                <li><span class="glyphicon glyphicon-ok"></span> Hedging Allowed</li>
                            </ul>
                        </div>
                        
                    </div>
                </div>
                <div class="row mt30 tc">   
                    <div class="lh50"><a href="http://user.agxfx.com/#/RealAccount" class="btn btn-success dib w300 h40 fw7 lh25">Open Live Account</a></div>
                    <div class="lh50"><a href="https://download.mql5.com/cdn/web/agx.forex.limited/mt4/agxforex4setup.exe" class="btn btn-danger  dib w300 h40 fw7 lh25">Download</a></div>
                    <!-- <div class="lh50"><a href="#" class="btn btn-default dib w300 h40 fw7 lh25 cc33">Open Demo Account</a></div> -->
                </div>
            </div>
        </div>
        <div class=" pt60 pb60 bcf1f1f1">   
            <div class="container"> 
                <div class="row">   
                    <div class="col-xs-12 ">    
                        <h3 class="lh35 ml30">How to install AGX MT4</h3>
                        <ul class="lSuperiority  ml30">    
                            <li class="cc33"><span class="glyphicon glyphicon-triangle-right cc33"></span> Download the terminal</li>
                            <li><span class="glyphicon glyphicon-triangle-right cc33"></span> Run the AGX.exe file after it has downloaded</li>
                            <li><span class="glyphicon glyphicon-triangle-right cc33"></span> When launching the program for the first time, you will see the login window</li>
                            <li><span class="glyphicon glyphicon-triangle-right cc33"></span> Enter your real or demo account login data</li>

                        </ul>
                    </div>
                </div>
            </div>
        </div>
        
        
        <!-- footer -->
        <?php include 'footer.html'; ?>        
        
    </div> <!-- wrapper -->


    <!-- js -->
    <script src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>   

    <script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <script src="assets/js/jw-base.js"></script>
</body>
</html>