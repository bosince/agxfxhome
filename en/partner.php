<!DOCTYPE HTML>
<html  lang="en">

<head>

    <meta charset="utf-8">

    <title>partner - AGX</title>

    <!-- meta -->
    <meta name="description" content="AGX ">

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=1" />

    <!-- favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.png">

    <!-- css -->
    <!-- <link rel="stylesheet" type="text/css" href="assets/css/normalize.css"> -->
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="assets/css/media.css">
    <!-- <link rel="stylesheet" type="text/css" href="assets/css/fonts.css"> -->
    <!-- <link rel="stylesheet" type="text/css" href="assets/css/jquery-owl-carousel/owl.carousel.css" /> -->
    <!-- <link rel="stylesheet" type="text/css" href="assets/css/jquery-loading/component.css" /> -->

    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="assets/css/jw-base.css" />
    <link href="https://cdn.bootcss.com/jquery.bootstrapvalidator/0.5.3/css/bootstrapValidator.min.css" rel="stylesheet">

    <!-- load modernizer -->
    <!-- <script type="text/javascript" src="assets/js/modernizr/modernizr-2.7.1.js"></script> -->

    <style>
        /*.art_style{font-family: "微软雅黑";}*/
        .container h3{line-height: 50px;font-weight: 700;}
        .container h4{line-height: 50px;font-weight: 700;}
        .container p{line-height: 30px;}

        .adv{background: url(assets/img/partner/adv_bg.jpg);}
        .serve ul {list-style-type: square;margin-left: 20px; margin-top: 50px; margin-bottom: 50px;}
        .serve ul li{font-weight: 700;height: 40px;}
        .z9{z-index: 9;}

        @media (max-width: 992px) {
            .max992-mc{margin: 0 auto;}
            .adv .max992-lh26{line-height: 26px;}
        }
    </style>

</head>

<body>


    <div id="wrapper">

        <!-- header -->
        <?php include 'header.html'; ?>

        <div class="slide h300 bcfff  p-r">
            <h2 class="cfff p-a t50- tc fs40  ffwryh">partner</h2>
        </div>

        <!-- <div class=" h500">


        </div>
 -->

        <div class="container  tc pt100 pb80" >

            <figure>
                <img src="assets/img/partner/02.png" alt="" class="w100-">
            </figure>
            <div class="art_style">
                <h3 class=" mt30">Customer transactions can be earned on commission.</h3>
                <div class="tl">
                    <p class="ti2">Increase your revenue by introducing new customers to us and earn commission each time they trade.</p>
                    <p class="ti2">AGX brought by international attaches great importance to your business, and is committed to provide our partners with the most attractive IB manager returns, at the same time, bring your customers market leading trading experience and the most transparent trading terms.</p>
                    <p class="ti2">The plan applies to all individuals and all sizes of business brokers, and you can only get rewards for introducing new clients to AGX international deals.</p>
                </div>
            </div>

        </div>
        <div class="adv tc">
            <div class="container ">
                <div class="row  pb50">
                    <div class="col-xs-6 col-md-3">
                        <figure>
                            <img src="assets/img/partner/11.png" alt="" class="mt50">
                        </figure>
                        <h4 class="max992-lh26">Attractive Commission</h4>
                        <p class="tl">The Commission returns the structure of our carefully designed, so that we all get sufficient reward agent,the more the amount of customer transactions, the more commission you earn.</p>
                    </div>
                    <div class="col-xs-6 col-md-3">
                        <figure>
                            <img src="assets/img/partner/12.png" alt="" class="mt50">
                        </figure>
                        <h4 class="max992-lh26">Advanced Reporting</h4>
                        <p class="tl">The advanced IB management center system ensures that agents at all levels receive fully visible and updated commission income and customer transactions details, and timely export reports to facilitate management</p>
                    </div>
                    <div class="col-xs-6 col-md-3">
                        <figure>
                            <img src="assets/img/partner/13.png" alt="" class="mt50">
                        </figure>
                        <h4 class="max992-lh26">Advanced Marketing Tools</h4>
                        <p class="tl">We provide you with free marketing tools and materials, including various icons and web pages, to customize your personalized application links, so that you can track, click, display, download and apply each user in a timely manner</p>
                    </div>
                    <div class="col-xs-6 col-md-3">
                        <figure>
                            <img src="assets/img/partner/14.png" alt="" class="mt50">
                        </figure>
                        <h4 class="max992-lh26">Senior Account Manager Support</h4>
                        <p class="tl">As an important partner of our company, you will have an individual, one to one, IB customer manager who will provide professional guidance for each step of your operation, and provide follow-up support for business development.</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="form">
            <div class="container ">
                <div class="row max1440-w850 dib pt50 pb50">
                    <div class="col-xs-12 col-md-6 ">
                        <img src="assets/img/partner/03.png" alt="" class="wow w100- max1440-w110">
                    </div>

                    <div class="col-xs-12 col-md-5 col-md-offset-1 tl wow ">
                        <div style="width: 194px;height: 76px;" class="max992-mc">
                            <img src="assets/img/partner/04.png" alt="" class="mt20 max1440-w80 ">
                        </div>

                        <form action="php/mail.php" method="post" class="mt50 " id="mailForm">
                            <div class="row ptb5 max1440-ptb0">
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <input type="text" name="name" class="form-control bgn" placeholder="Name" >
                                    </div>
                                </div>

                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <input type="tel" name="tel" class="form-control bgn" placeholder="Phone" >
                                    </div>
                                </div>
                            </div>
                            <div class="ptb5 max1440-ptb0">
                                <div class="form-group">
                                    <input type="email" name="email" class="form-control bgn" placeholder="Email">
                                </div>
                            </div>
                            <div class="ptb5 max1440-ptb0">
                                <div class="form-group">
                                    <textarea class="form-control bgn" name="need" rows="3" placeholder="Your needs"></textarea>
                                </div>
                            </div>
                            <div class="row ptb5 max1440-ptb0">
                                <div class="col-xs-6">
                                    <!-- <input type="text" name="name" class="form-control" placeholder="姓名"> -->
                                    <button type="submit" class="btn btn-default form-control bc333 bn cfff"  >  Submit  </button>
                                </div>
                            </div>

                        </form>

                    </div>
                </div>

                <div>
                    <img src="assets/img/partner/line.png" alt="" class="w100-">
                </div>
                <h3 class="mt50">Your custom will get the following services</h3>
                <p>When you become our partner, you and your customers will receive the most AGX International provides quality service:</p>
                <div class="row  serve">
                    <div class="col-xs-12 col-sm-6">
                        <ul>
                            <li>The most extensive global trading market</li>
                            <li>Numerous and high praise platform trading systems</li>
                            <li>Liquidity and strong execution speed</li>
                            <li>24 hours of professional customer service technical support</li>
                            <li>Dealing with the most reliable compliance broker</li>
                        </ul>
                    </div>
                    <div class="col-sm-6 p-r hidden-xs">
                        <figure class="p-a l-40 w500 t-100">
                            <img src="assets/img/partner/05.png" alt="" class="w100-">
                        </figure>
                    </div>
                </div>

            </div>
        </div>

        <!-- footer -->
        <?php include 'footer.html'; ?>

    </div> <!-- wrapper -->

    <!-- js -->
    <script src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>

    <script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="assets/js/jw-base.js"></script>
    <script src="https://cdn.bootcss.com/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.min.js"></script>
    <script>
        $(function(){
            $("#mailForm").bootstrapValidator({
                message: 'This value is not valid',
            　　validfeedbackIcons: {
    　　　　　　　　valid: 'glyphicon glyphicon-ok',
    　　　　　　　　invalid: 'glyphicon glyphicon-remove',
    　　　　　　　　validating: 'glyphicon glyphicon-refresh'
　　　　　　　　},
                fields: {
                    name: {
                        validators: {
                            notEmpty: {
                                message: 'user name cannot be empty'
                            },
                            stringLength: {
                                min: 3,
                                max: 15,
                                message: 'the user name is wrong'
                            }

                        }
                    },
                    email: {
                        validators: {
                            notEmpty: {
                                message: 'email cannot be empty'
                            },
                            emailAddress: {
                                message: 'the email is wrong '
                            }
                        }
                    },
                    tel: {
                        validators: {
                            notEmpty: {
                                message: 'telephone number cannot be empty'
                            },
                            stringLength: {
                                 min: 11,
                                 max: 11,
                                 message: 'phone number is wrong'
                             },
                             regexp: {
                                 regexp: /^1[3|5|8]{1}[0-9]{9}$/,
                                 message: 'phone number is wrong'
                             }
                        }
                    },
                    need: {
                        validators: {
                            notEmpty: {
                                message: 'need cannot be empty'
                            },
                            stringLength: {
                                 min: 3,
                                 max: 200,
                                 message: 'error'
                             }

                        }
                    }
                }
            })
        })
    </script>
</body>
</html>
