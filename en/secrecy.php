<!DOCTYPE HTML>
<html>

<head>
    
    <meta charset="utf-8">
    
    <title>secrecy agreement - AGX</title>
    
    <!-- meta -->
    <meta name="description" content="AGX ">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=1" />
    
    <!-- favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.png">

    <!-- css -->
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="assets/css/jw-base.css" />
    
    <!-- load modernizer -->
    <!-- <script type="text/javascript" src="assets/js/modernizr/modernizr-2.7.1.js"></script> -->
    
    <style>
        .art_style h3{line-height: 50px;}
        .art_style p{line-height: 30px;}


    </style>

</head>

<body>
    
    
    <div id="wrapper">
        
        <!-- header -->
        <?php include 'header.html'; ?>

        <div class="slide h300 bcfff  tc pt120">
            <h2 class="cfff fs40  ffwryh">secrecy agreement</h2>
        </div>


        <div class="container  ">
            <div class="ffwryh c666 art_style mt50 mb50">

                <h3>Personal Information</h3>
                <p>When a Client asks for information about AGX's products and services, accesses the AGX website, or submits an application to open an account with AGX, they may be providing AGX with personal information.</p>

                <p>AGX will maintain records of all transactions and activities on accounts with AGX, including details of contracts traded and margin calls made. During the course of a relationship with AGX, information about products and services provided utilised by the Client will be kept on record.</p>

                <p>When assessing a Client application, AGX may also collect information about the prospective Client from publicly available sources.</p>

                <p>Client personal information will be treated strictly in accordance with the National Privacy Principles in the Australian Privacy Act. An Australian resident customer may at any time, upon request, gain access to the information that the AGX holds about him or her in accordance with the National Privacy Principles as described below.</p>


                <h3>Use of Personal Information</h3>
                <p>The information requested in the Account Application to open an account is required by AGX to determine whether a prospective Client has enough knowledge and experience to trade in off-exchange, Over-the-Counter derivatives with AGX. That information, together with the information collected and maintained by AGX for duration of an account, is required to keep Clients informed in regards to their account status, margin obligations, and trading activities.</p>

                <p>The information requested by AGX when accessing our website or completing one of our Account Applications is to allow us to provide our Clients with information regarding the products and services offered by AGX that best suit their investment needs and risk appetite. AGX takes all reasonable steps to protect Client's personal information from misuse, loss, unauthorised access, modification or disclosure.</p>


                <h3>Telephone Conversations</h3>
                <p>AGX may also record telephone conversations between the Client and AGX's authorised representatives. Such recordings, or transcripts from such recordings, may be used to resolve any Client dispute. Recordings or transcripts made by AGX of Client telephone conversations may be erased at AGX's discretion.</p>


                <h3>Website</h3>
                <p>AGX collects statistical information about visitors to our websites such as the number of visitors, pages viewed, types of transactions conducted, time online and documents downloaded. This information is used to evaluate and improve the performance of our websites. Other than statistical information, we do not collect any personal information through our website unless provided to us.</p>


                <h3>Updating Personal Information</h3>
                <p>AGX asks that Clients promptly notify the company of any changes to the personal information on file. This allows AGX to keep Clients informed regarding their accounts, margin obligations, and trading activities. You may ask us at any time to correct personal information held by AGX that is outdated or inaccurate. Should we disagree with you as to the accuracy of the information, you may request that we attach a statement to that information noting that you consider it inaccurate or incomplete.</p>


                <h3>Client Consent</h3>
                <p>By accessing AGX's website the Client consents to the collecting, maintaining, using and disclosing personal information provided.</p>

            </div>
        </div>

        
        <!-- footer -->
        <?php include 'footer.html'; ?>
        
    </div> <!-- wrapper -->

    <!-- js -->
    <script src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>   
    <script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="assets/js/jw-base.js"></script>
</body>
</html>