<!DOCTYPE HTML>
<html>

<head>

    <meta charset="utf-8">

    <title>AGX—加盟优势</title>
    <meta name="keywords" content="加盟优势,金融监管牌照,流动性提供商,透明报价,超快速开户,畅通银联出入金、快速指令执行 "/>
     <meta name="description" content=" AGX总部,位于欧洲最大的经济中心——英国伦敦,作为世界上最大的国际外汇市场 和世界上最大的离岸美元、离岸欧元市场,美元和欧元正是在这里定价,全球41%的货币业务都是在伦敦交易完成！">

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=1" />

    <!-- favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.png">

    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="assets/css/jw-base.css" />

    <!-- load modernizer -->
    <script type="text/javascript" src="assets/js/modernizr/modernizr-2.7.1.js"></script>

    <style>
        .media-img{width: 200px;}
        .media-title{padding-top: 30px;}
        .media-list{
            padding-top: 30px;
            margin-bottom: 50px;
            margin-left: 30px;
            margin-right: 30px;
        }
        .media-list h4{
            font-weight: 700;
            font-size: 20px;
            line-height: 35px;
        }
        .container p{
            font-size: 15px;
            line-height: 28px;
        }
        .media-body{padding-left: 30px;}
        @media (max-width: 1440px) {
            .max1440-w920{width: 920px;}

        }

        @media (max-width: 768px){
            .media-img{width: 100px;}
            .media-title{padding-top: 30px;}
            .media-list{
                padding-top: 30px;
                margin-bottom: 50px;
                margin-left: 0;
                margin-right: 0;
            }
        }
        

    </style>

</head>

<body>


    <div id="wrapper">

        <!-- header -->
        <?php include 'header.html'; ?>

        <div class="slide h300 bcfff tc pt120">
            <h2 class="cfff fs40  ffwryh">加盟优势</h2>
        </div>



        <div  >
        <div class="container  ">
            <!-- <h4 class="fs30 fw7 pt30 mb30 lh60 c333 ffht  tc">关于我们</h4> -->
            <p class="mt50">AGX作为一家国际专业的外汇金融交易平台服务商，一直致力于利用金融领域高端科技产品，依托国际级服务水准和专业化的营销团队，为合作伙伴提供灵活多样的经营模式和合作方案、并进行各种产品组合、交易机制的打造，以及客户服务支持，帮助合作伙伴迅速创立更加合理的营销方式，提高公司盈利。</p>
            <ul class="media-list ">
                <li class="media">
                    <div class="media-left media-middle">
                        <img class="media-object media-img" src="assets/img/about/adv/1.jpg" alt="...">
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading media-title">全方位投资品种支持</h4>
                        <p>AGX为合作伙伴提供多元化及全面的投资品种，其中包括外汇，贵金属，原油，美股，期货等产品，方便合作伙伴服务不同需求的客户。</p>
                    </div>
                </li>
                <li class="media">
                    <div class="media-left media-middle">
                        <img class="media-object media-img" src="assets/img/about/adv/2.jpg" alt="...">
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading media-title">全方位分成计划支持</h4>
                        <p>凭借在外汇行业多年的成熟运作经验， AGX形成了具有竞争力的代理分成计划，采用能够灵活高效的返佣制度，加上与合作伙伴的分享红利体系，帮助合作伙伴更好地扩展业务。</p>
                    </div>
                </li>
                <li class="media">
                    <div class="media-left media-middle">
                        <img class="media-object media-img" src="assets/img/about/adv/3.jpg" alt="...">
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading media-title">全方位智能后台支持</h4>
                        <p>凭借在外汇行业多年的成熟运作经验， AGX形成了具有竞争力的代理分成计划，采用能够灵活高效的返佣制度，加上与合作伙伴的分享红利体系，帮助合作伙伴更好地扩展业务。</p>
                    </div>
                </li>
                <li class="media">
                    <div class="media-left media-middle">
                        <img class="media-object media-img" src="assets/img/about/adv/4.jpg" alt="...">
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading media-title">全方位营销策略支持</h4>
                        <p>AGX擅长因地制宜的服务于合作伙伴，为每一位合作伙伴提供量身定制的合作方案，以及包括讲座、培训、媒体广告、市场推广等营销策略支持，帮助合作伙伴更有针对性的拓展市场。。</p>
                    </div>
                </li>
                <li class="media">
                    <div class="media-left media-middle">
                        <img class="media-object media-img" src="assets/img/about/adv/5.jpg" alt="...">
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading media-title">全方位业务培训支持</h4>
                        <p>针对合作伙伴在行业理解程度上的不同和营销方式上的差异，AGX为合作伙伴提供各类专业培训服务，包括外汇知识、交易技能、运营管理、售后服务等，帮助合作伙伴更好的开展业务。</p>
                    </div>
                </li>
                <li class="media">
                    <div class="media-left media-middle">
                        <img class="media-object media-img" src="assets/img/about/adv/6.jpg" alt="...">
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading media-title">全方位硬件数据支持 </h4>
                        <p>AGX在全球范围内部署多台高配置服务器，并建立了庞大的云数据中心，帮助全球和国内投资者们高速桥接国际银行系统，消除延迟至毫秒级别内，快速订单成交</p>
                    </div>
                </li>
                <li class="media">
                    <div class="media-left media-middle">
                        <img class="media-object media-img" src="assets/img/about/adv/7.jpg" alt="...">
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading media-title">全方位专属客服支持</h4>
                        <p>AGX为全球包括中国的合作伙伴开辟畅通的专属客服服务，并设立多种语言客服，帮助全球范围内的合作伙伴分析经营中遇到的各种问题，并提供专业的问题解决建议。</p>
                    </div>
                </li>
            </ul>
        </div>
    </div>

        <!-- footer -->
        <?php include 'footer.html'; ?>

    </div> <!-- wrapper -->

    <!-- js -->
    <script src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>

    <script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <script src="assets/js/jw-base.js"></script>

</body>
</html>
