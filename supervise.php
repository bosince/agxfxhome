<!DOCTYPE HTML>
<html>

<head>
    
    <meta charset="utf-8">
    
    <title> AGX—监管资质</title>
    <meta name="keywords" content="监管,FCA,NFA,OTC,DMA"/>
    <meta name="description" content=" AGX受英国FCA监管|AGX受美国NFA监管|AGX提供集中撮合期权交易及外汇、差价合约、商品期货|AGX办事机构遍布全球十多个国家">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=1" />
    
    <!-- favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.png">

    <!-- css -->
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="assets/css/jw-base.css" />
    
    <!-- load modernizer -->
    <script type="text/javascript" src="assets/js/modernizr/modernizr-2.7.1.js"></script>
    
    <style>
        @media (max-width: 1440px) {
            .max1440-w920{width: 920px;}

        }

        @media (max-width: 1000px){
            .max1000-w100-{width: 100%}
            .max1000-plr15{padding-left: 15px;padding-right: 15px;}
        }

    </style>

</head>

<body>
    
    
    <div id="wrapper">
        
        <!-- header -->
        <?php include 'header.html'; ?>

        <div class="slide h300 bcfff tc pt120">
            <h2 class="cfff fs40  ffwryh">安全监管</h2>
            <!-- <h4 class="fs20  mb30  c333  tc">SECURITY SUPERVISION</h4> -->
        </div>


        
        <div   >
            <div class="container tc ffwryh fs16 ">
                <!-- <h4 class="fs40 fw7 pt50  lh60 c333  tc">安全监管</h4> -->
                <!-- <h4 class="fs20  mb30  c333  tc">SECURITY SUPERVISION</h4> -->
                <div class="tc mt50">
                    <img src="assets/img/supervise/01.png" alt="" class="max1000-w100-">
                    <!-- <img src="assets/img/supervise/02.png" alt="" class="mt30"> -->
                </div>
                <p class="dib fs22 mt10">拥有总部AGX旗下的两大监管体制</p>
                <div class="row pt30">
                    <div class="col-xs-6">
                        <img src="assets/img/supervise/03.png" alt="" class="max1000-w100-">
                    </div>
                    <div class="col-xs-6">
                        <img src="assets/img/supervise/04.png" alt="" class="max1000-w100-">
                    </div>
                </div>
                <div class="row mt30 tl mb30">
                    <div class="col-xs-6 br1s666">
                        <p class="ti2 lh25 pr20 pl30 max1000-plr15">不同于 OTC 市场许多传统的公司，AGX金融更专注于向 OTC 市场的机构客户与合作伙伴提供定制的交易平台解决方案，包括 DMA 直接市场接入模式、风险管理与 API 流动性。同时，AGX金融也向专业客户提供定制的交易执行。</p>
                    </div>
                    <div class="col-xs-6 ">
                        <ul class="ul1 pl50 pr20 max1000-plr15">
                            <li>AGX受英国FCA监管</li>
                            <li>AGX受美国NFA监管</li>
                            <li>AGX提供集中撮合期权交易及外汇、差价合约、商品期货</li>
                            <li>AGX办事机构遍布全球十多个国家</li>
                        </ul>
                    </div>
                </div>
                <img src="assets/img/supervise/05.png" alt="" class="mt80 w100-">
                <div>
                    <h4 class="fs40 fw7 pt50  lh60 c333  tc">监管优势</h4>
                    <div class="row mt50">
                        <div class="col-xs-6 col-md-1-5">
                            <div>
                                <img src="assets/img/supervise/07.png" alt="">
                            </div>
                            <div class="mt30">
                                <p class="lh25">保障投资者需要及</p>
                                <p class="lh25">提高金融机构内部监管</p>
                                <p class="lh25">以及市场透明度</p>
                            </div>
                        </div>
                        <div class="col-xs-6 col-md-1-5"><div>
                                <img src="assets/img/supervise/08.png" alt="">
                            </div>
                            <div class="mt30">
                                <p class="lh25">英国FCA拥有完善</p>
                                <p class="lh25">的金融监管体系和严格</p>
                                <p class="lh25">的执行力度</p>
                            </div>
                        </div>
                        <div class="col-md-1-5 col-xs-4 ">
                            <div>
                                <img src="assets/img/supervise/09.png" alt="">
                            </div>
                            <div class="mt30">
                                <p class="lh25">美国NFA是世界上最</p>
                                <p class="lh25">严格、最能保护投资者</p>
                                <p class="lh25">权益的金融监管体系</p>
                            </div>
                        </div>
                        <div class=" col-md-1-5 col-xs-4 ">
                            <div>
                                <img src="assets/img/supervise/10.png" alt="">
                            </div>
                            <div class="mt30">
                                <p class="lh25">让投资者可</p>
                                <p class="lh25">以于稳健的金融市场</p>
                                <p class="lh25">进行投资活动</p>
                            </div>
                        </div>

                        <div class="col-md-1-5 col-xs-4 ">
                            <div>
                                <img src="assets/img/supervise/11.png" alt="">
                            </div>
                            <div class="mt30">
                                <p class="lh25">英国FCA</p>
                                <p class="lh25">和美国NFA双牌照，投</p>
                                <p class="lh25">资者的双重保险</p>
                            </div>
                        </div>
                    </div>
                </div>
                <img src="assets/img/supervise/05.png" alt="" class="mt80 w100-"  >
                <div class="row mt100 mb100">
                    <div class="col-sm-6 hidden-xs">
                        <img src="assets/img/supervise/06.jpg" alt="" class="max1440-w100">
                    </div>
                    <div class="col-xs-12 col-sm-6 tl">
                        <h4 class="fs40 fw7  lh60 c333 max1440-fs30 max1440-mt-20">争端解决计划 DRS</h4>
                        <h4 class="fs20  mb10  c333 ">dispte resolution scheme</h4>
                        <div>
                            <p class="lh25">AGX拥有英国FCA和美国NFA双重监管</p>
                            <p class="lh25">同时AGX已加入英国金融服务补偿计划（FSCS）</p>
                            <p class="lh25">若发生交易意外 </p>
                            <p class="lh25">投资者将获得五万英镑的补偿 </p>
                            <p class="lh25">并有可能将此补偿全额上调 </p>
                            <p class="lh25">同时AGX也加入了美国商品与期货交易委员会（CFTC）</p>
                            <p class="lh25">CFTC作为一个独立的机构 </p>
                            <p class="lh25">主要职责和作用是保护市场参与者和投资者</p>
                            <p class="lh25">保障期货和期权市场的开放性、竞争性的和财务上的可靠性</p>
                        </div>

                        <h4 class="fs40 fw7 pt50  lh60 c333 mt50 max1440-mt-20 max1440-fs30">反洗钱政策</h4>
                        <h4 class="fs20  mb10  c333 ">Against Money Laundering</h4>
                        <p  class="lh25">对于一系列的违规FCA和NFA,所有持牌公司必须具备KYC（了解你的客户）和AML（反洗钱）的相关公司内审政策和法律要求。对具体客户风险评估，反洗钱和反恐怖主义金融进行规定，并规定了具体的要求。</p>
                        
                    </div>
                </div>

                
            </div>
        </div>
        
        <!-- footer -->
        <?php include 'footer.html'; ?>
        
    </div> <!-- wrapper -->

    <!-- js -->
    <script src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>   
    <script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="assets/js/jw-base.js"></script>
</body>
</html>