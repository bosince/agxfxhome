<!DOCTYPE HTML>
<html>

<head>
    
    <meta charset="utf-8">
    
    <title> AGX—货币交易品种一览表</title>
    <meta name="keywords" content="外汇主流盘,交易品种,货币对"/>
     <meta name="description" content="AGX采用浮动点差，客户能在AGX平台中体验到令人兴奋的至低点差，且无重复报价。此外，AGX还能提供高达100：1的灵活杠杆，有效帮助交易者实现利益最大化。">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=1" />
    
    <!-- favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.png">
    
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="assets/css/jw-base.css" />
    
    <style> 
        h2{font-size: 35px;}
        p{line-height: 32px;font-size: 15px;color: #666}
        .table th, .table td{text-align: center;}

    </style>

</head>

<body>
    
    
    <div id="wrapper" class="ffwryh">
        
        <!-- header -->
        <?php include 'header.html'; ?>
        <div class="container">
            <ol class="breadcrumb bcfff lh50 mb0">
                <li><a href="/">首页</a></li>
                <li><a href="/pro-forex.php">外汇主流盘</a></li>
                <li class="active">货币交易品种一览表</li>
            </ol>
        </div>

        <div class="data-box pt80 pb80">
            <div class="container">
                <h2 class="tc">货币交易品种一览表</h2>
                <p class="tc plr15 mt30">AGX采用浮动点差，客户能在AGX平台中体验到令人兴奋的至低点差，且无重复报价。此外，AGX还能提供高达100：1的灵活杠杆，有效帮助交易者实现利益最大化。</p>
                <div class="table-responsive">
                
                    <table class="table table-bordered table-striped table-hover c666 mt30" >
                        <tr>
                            <th>热门货币对</th>
                            <th>最小交易手数</th>
                            <th>最大交易手数</th>
                            <th>合约单位</th>
                            <th>挂单距离</th>
                            <th>保证金比例<br>（视账户净值及余额情况而定）</th>
                            <th>交易时间（北京时间）</th>
                            <th>交易时间（MT4时间）</th>
                        </tr>
                        <tbody>
                            <tr>
                                <td>EUR/USD&nbsp;</td>
                                <td>0.01</td>
                                <td>100</td>
                                <td>100,000</td>
                                <td>5</td>
                                <td>0.5%~2%</td>
                                <td>周一至周五：06:05-05:59</td>
                                <td>周一至周五：00:05-24:00</td>
                            </tr>
                            <tr>
                                <td>USD/JPY&nbsp;</td>
                                <td>0.01</td>
                                <td>100</td>
                                <td>100,000</td>
                                <td>5</td>
                                <td>0.5%~2%</td>
                                <td>周一至周五：06:05-05:59</td>
                                <td>周一至周五：00:05-24:00</td>
                            </tr>
                            <tr>
                                <td>GBP/USD&nbsp;</td>
                                <td>0.01</td>
                                <td>100</td>
                                <td>100,000</td>
                                <td>5</td>
                                <td>0.5%~2%</td>
                                <td>周一至周五：06:05-05:59</td>
                                <td>周一至周五：00:05-24:00</td>
                            </tr>
                            <tr>
                                <td>USD/CHF&nbsp;</td>
                                <td>0.01</td>
                                <td>100</td>
                                <td>100,000</td>
                                <td>5</td>
                                <td>0.5%~2%</td>
                                <td>周一至周五：06:05-05:59</td>
                                <td>周一至周五：00:05-24:00</td>
                            </tr>
                            <tr>
                                <td>AUD/USD&nbsp;</td>
                                <td>0.01</td>
                                <td>100</td>
                                <td>100,000</td>
                                <td>5</td>
                                <td>0.5%~2%</td>
                                <td>周一至周五：06:05-05:59</td>
                                <td>周一至周五：00:05-24:00</td>
                            </tr>
                            <tr>
                                <td>NZD/USD&nbsp;</td>
                                <td>0.01</td>
                                <td>100</td>
                                <td>100,000</td>
                                <td>5</td>
                                <td>0.5%~2%</td>
                                <td>周一至周五：06:05-05:59</td>
                                <td>周一至周五：00:05-24:00</td>
                            </tr>
                            <tr>
                                <td>USD/CAD&nbsp;</td>
                                <td>0.01</td>
                                <td>100</td>
                                <td>100,000</td>
                                <td>5</td>
                                <td>0.5%~2%</td>
                                <td>周一至周五：06:05-05:59</td>
                                <td>周一至周五：00:05-24:00</td>
                            </tr>
                            <tr>
                                <td>EUR/GBP&nbsp;</td>
                                <td>0.01</td>
                                <td>20</td>
                                <td>100,000</td>
                                <td>9.9</td>
                                <td>0.5%~2%</td>
                                <td>周一至周五：06:05-05:59</td>
                                <td>周一至周五：00:05-24:00</td>
                            </tr>
                            <tr>
                                <td>EUR/CHF</td>
                                <td>0.01</td>
                                <td>20</td>
                                <td>100,000</td>
                                <td>9.9</td>
                                <td>0.5%~2%</td>
                                <td>周一至周五：06:05-05:59</td>
                                <td>周一至周五：00:05-24:00</td>
                            </tr>
                            <tr>
                                <td>EUR/JPY</td>
                                <td>0.01</td>
                                <td>100</td>
                                <td>100,000</td>
                                <td>5</td>
                                <td>0.5%~2%</td>
                                <td>周一至周五：06:05-05:59</td>
                                <td>周一至周五：00:05-24:00</td>
                            </tr>
                            <tr>
                                <td>GBP/JPY&nbsp;</td>
                                <td>0.01</td>
                                <td>100</td>
                                <td>100,000</td>
                                <td>5</td>
                                <td>0.5%~2%
                                </td>
                                <td>周一至周五：06:05-05:59</td>
                                <td>周一至周五：00:05-24:00</td>
                            </tr>
                            <tr>
                                <td>CHF/JPY&nbsp;</td>
                                <td>0.01</td>
                                <td>100</td>
                                <td>100,000</td>
                                <td>5</td>
                                <td>0.5%~2%</td>
                                <td>周一至周五：06:05-05:59</td>
                                <td>周一至周五：00:05-24:00</td>
                            </tr>
                            <tr>
                                <td>AUD/JPY</td>
                                <td>0.01</td>
                                <td>100</td>
                                <td>100,000</td>
                                <td>5</td>
                                <td>0.5%~2%</td>
                                <td>周一至周五：06:05-05:59</td>
                                <td>周一至周五：00:05-24:00</td>
                            </tr>
                            <tr>
                                <td>NZD/JPY</td>
                                <td>0.01</td>
                                <td>100</td>
                                <td>100,000</td>
                                <td>5</td>
                                <td>0.5%~2%</td>
                                <td>周一至周五：06:05-05:59</td>
                                <td>周一至周五：00:05-24:00</td>
                            </tr>
                            <tr>
                                <td>CAD/JPY</td>
                                <td>0.01</td>
                                <td>20</td>
                                <td>100,000</td>
                                <td>9.9</td>
                                <td>0.5%~2%</td>
                                <td>周一至周五：06:05-05:59</td>
                                <td>周一至周五：00:05-24:00</td>
                            </tr>
                            <tr>
                                <td>AUD/NZD</td>
                                <td>0.01</td>
                                <td>20</td>
                                <td>100,000</td>
                                <td>9.9</td>
                                <td>0.5%~2%</td>
                                <td>周一至周五：06:05-05:59</td>
                                <td>周一至周五：00:05-24:00</td>
                            </tr>
                            <tr>
                                <td>EUR/AUD&nbsp;</td>
                                <td>0.01</td>
                                <td>20</td>
                                <td>100,000</td>
                                <td>9.9</td>
                                <td>0.5%~2%</td>
                                <td>周一至周五：06:05-05:59</td>
                                <td>周一至周五：00:05-24:00</td>
                            </tr>
                            <tr>
                                <td>EUR/CAD</td>
                                <td>0.01</td>
                                <td>20</td>
                                <td>100,000</td>
                                <td>9.9</td>
                                <td>0.5%~2%</td>
                                <td>周一至周五：06:05-05:59</td>
                                <td>周一至周五：00:05-24:00</td>
                            </tr>
                            <tr>
                                <td>GBP/CHF&nbsp;</td>
                                <td>0.01</td>
                                <td>20</td>
                                <td>100,000</td>
                                <td>9.9</td>
                                <td>0.5%~2%</td>
                                <td>周一至周五：06:05-05:59</td>
                                <td>周一至周五：00:05-24:00</td>
                            </tr>
                            <tr>
                                <td>GBP/AUD</td>
                                <td>0.01</td>
                                <td>20</td>
                                <td>100,000</td>
                                <td>9.9</td>
                                <td>0.5%~2%</td>
                                <td>周一至周五：06:05-05:59</td>
                                <td>周一至周五：00:05-24:00</td>
                            </tr>
                            <tr>
                                <td>GBP/CAD</td>
                                <td>0.01</td>
                                <td>20</td>
                                <td>100,000</td>
                                <td>9.9</td>
                                <td>0.5%~2%</td>
                                <td>周一至周五：06:05-05:59</td>
                                <td>周一至周五：00:05-24:00</td>
                            </tr>
                            <tr>
                                <td>GBP/NZD&nbsp;</td>
                                <td>0.01</td>
                                <td>20</td>
                                <td>100,000</td>
                                <td>9.9</td>
                                <td>0.5%~2%</td>
                                <td>周一至周五：06:05-05:59</td>
                                <td>周一至周五：00:05-24:00</td>
                            </tr>
                            <tr>
                                <td>EUR/NZD</td>
                                <td>0.01</td>
                                <td>20</td>
                                <td>100,000</td>
                                <td>9.9</td>
                                <td>0.5%~2%</td>
                                <td>周一至周五：06:05-05:59</td>
                                <td>周一至周五：00:05-24:00</td>
                            </tr>
                            <tr>
                                <td>AUD/CHF</td>
                                <td>0.01</td>
                                <td>20</td>
                                <td>100,000</td>
                                <td>19.9</td>
                                <td>0.5%~2%</td>
                                <td>周一至周五：06:05-05:59</td>
                                <td>周一至周五：00:05-24:00</td>
                            </tr>
                            <tr>
                                <td>AUD/CAD</td>
                                <td>0.01</td>
                                <td>20</td>
                                <td>100,000</td>
                                <td>19.9</td>
                                <td>0.5%~2%</td>
                                <td>周一至周五：06:05-05:59</td>
                                <td>周一至周五：00:05-24:00</td>
                            </tr>
                            <tr>
                                <td>CAD/CHF</td>
                                <td>0.01</td>
                                <td>20</td>
                                <td>100,000</td>
                                <td>19.9</td>
                                <td>0.5%~2%</td>
                                <td>周一至周五：06:05-05:59</td>
                                <td>周一至周五：00:05-24:00</td>
                            </tr>
                            <tr>
                                <td>NZD/CAD</td>
                                <td>0.01</td>
                                <td>20</td>
                                <td>100,000</td>
                                <td>19.9</td>
                                <td>0.5%~2%</td>
                                <td>周一至周五：06:05-05:59</td>
                                <td>周一至周五：00:05-24:00</td>
                            </tr>
                            <tr>
                                <td>NZD/CHF</td>
                                <td>0.01</td>
                                <td>20</td>
                                <td>100,000</td>
                                <td>19.9</td>
                                <td>0.5%~2%
                                </td>
                                <td>周一至周五：06:05-05:59</td>
                                <td>周一至周五：00:05-24:00</td>
                            </tr>
                        
                        </tbody>
                    </table>
                    
                </div>

                <p>注：*美国夏令时，北京交易时间相应提前1小时<br>AGX 提醒您考虑提高杠杆率的风险。市场上相对较小的波动可能按比例放大，对您已存入或将要存入的资金产生较大影响，这可能对您不利，也可能对您有利。您可能损失全部原始保证金，并需要存入额外资金来补仓。</p>
                
            </div>
        </div>

        
        <!-- footer -->
        <?php include 'footer.html'; ?>        
        
    </div> <!-- wrapper -->


    <!-- js -->
    <script src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>   

    <script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <script src="assets/js/jw-base.js"></script>
</body>
</html>